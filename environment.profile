JAVA_HOME=${frontEnd.javaHome}

DATABASE_TNS_NAME=${backEnd.database.tnsName}
DATABASE_USERNAME=${backEnd.database.username}
DATABASE_PASSWORD=${backEnd.database.password}

SVN_REPOSITORY=${frontEnd.svn.url}
SVN_USERNAME=${frontEnd.svn.username}
SVN_PASSWORD=${frontEnd.svn.password}

WORKDIR=${backEnd.workDir}
